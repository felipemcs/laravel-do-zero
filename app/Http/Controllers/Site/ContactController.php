<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Models\Contact;
use App\Notifications\NewContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\View\View;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('site.contact.index');
    }

    /**
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function form(ContactFormRequest $request)
    {
        $contact = Contact::create($request->all());

        Notification::route('mail', config('mail.from.address'))
            ->notify(new NewContact($contact));

        toastr()->success('O contato foi criado com sucesso.');

        return back();
    }
}
