<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome não pode ser vazio.',
            'email.required' => 'O e-mail não pode ser vazio.',
            'email.email' => 'O e-mail precisa ser um endereço de e-mail válido.',
            'message.required' => 'A mensagem não pode ser vazia.',
        ];
    }
}
