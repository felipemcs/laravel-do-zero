<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ContactFormTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testVisitContactPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/contato')
                    ->assertSee('Contato');
        });
    }

    public function testInputs()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/contato')
                ->assertSee('Nome completo')
                ->assertSee('Email')
                ->assertSee('Mensagem');
        });
    }

    public function testFillAndSubmit()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/contato')
                ->type('name', 'Felipe Testee')
                ->type('email', 'felipec@fdssfd.com')
                ->type('message', 'mensagem automatizada')
                ->press('Enviar mensagem')
                ->waitFor('.toast-message')
                ->assertPathIs('/contato')
                ->assertSee('O contato foi criado com sucesso.');
        });
    }
}
